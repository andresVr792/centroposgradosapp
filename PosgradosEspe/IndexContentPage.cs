﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PosgradosEspe
{
    class IndexContentPage:ContentPage
    {
        ProgressBar progressbar;
        public IndexContentPage()
        {

            Button start = new Button
            {
                Text = "Ingresar",
                Font = Font.SystemFontOfSize(NamedSize.Medium),
                BorderRadius = 15,
                BorderWidth = 3,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                BackgroundColor = Color.FromHex("002e6d"),
                TextColor = Color.White,


            };

            progressbar = new ProgressBar
            {
                VerticalOptions = LayoutOptions.CenterAndExpand,
                IsVisible = false
            };

            Button exit = new Button
            {
                Text = "Salir",
                Font = Font.SystemFontOfSize(NamedSize.Medium),
                BorderRadius = 15,
                BorderWidth = 3,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                BackgroundColor = Color.FromHex("C0392B"),
                TextColor = Color.White,



            };

            exit.Clicked += OnExit;
            start.Clicked += OnStart;





            Image logoPosgrados = new Image
            {
                Aspect = Aspect.AspectFit,
                VerticalOptions = LayoutOptions.FillAndExpand

            };

            logoPosgrados.Source = ImageSource.FromFile("principal.png");


            var butonContent = new StackLayout
            {

                Orientation = StackOrientation.Horizontal,
                Padding = new Thickness(0, 10),
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                Children ={
                     start,
                     exit
                }
            };
            async void OnExit(object sender, EventArgs e)
            {
                if (await this.DisplayAlert("Centro de Posgrados", "Gracias vuelva pronto!", "Aceptar", "Cancelar"))
                {

                }
            }

            async void OnStart(object sender, EventArgs e)
            {
                this.progressbar.IsVisible = true;
                if (await progressbar.ProgressTo(1, 5550, Easing.Linear))
                {



                }

            }
            this.Content = new StackLayout
            {
                Padding = 30,
                Margin = 40,
                Spacing = 10,
                Children =
                {

                   logoPosgrados,
                   butonContent,
                   progressbar

                }
            };
        }
    }
}
